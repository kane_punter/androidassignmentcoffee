package com.example.kanepunter.coffeeapplication;


import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class OrderPageFragment extends Fragment {

    Spinner drinkItemsSpinner, drinkSizeSpinner, milkAmountSpinner, sugarAmountSpinner;
    Button addToOrderBtn;


    public OrderPageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View orderPageView = inflater.inflate(R.layout.fragment_order_page, container, false);



        //Spinners
        drinkItemsSpinner = (Spinner) orderPageView.findViewById(R.id.select_drinks_spinner);
        drinkSizeSpinner = (Spinner) orderPageView.findViewById(R.id.drink_size_spinner);
        milkAmountSpinner = (Spinner) orderPageView.findViewById(R.id.milk_spinner);
        sugarAmountSpinner = (Spinner) orderPageView.findViewById(R.id.sugars_spinner);
        addToOrderBtn = (Button)orderPageView.findViewById(R.id.add_item_to_order_btn);



        drinkItemsSpinner.post(new Runnable() {
            @Override
            public void run() {
                drinkItemsSpinner.setOnItemSelectedListener(drinkSelectedListener);
            }
        });

        drinkSizeSpinner.post(new Runnable() {
            @Override
            public void run() {
                drinkSizeSpinner.setOnItemSelectedListener(sizeSelectedListener);
            }
        });

        milkAmountSpinner.post(new Runnable() {
            @Override
            public void run() {
                milkAmountSpinner.setOnItemSelectedListener(milkSelectedListener);
            }
        });
        sugarAmountSpinner.post(new Runnable() {
            @Override
            public void run() {
                sugarAmountSpinner.setOnItemSelectedListener(sugarSelectedListener);
            }
        });


//        fab = (FloatingActionButton)  orderPageView.findViewById(R.id.fab_order);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, ("Your Order. A " + drinkSizeSpinner.getSelectedItem().toString() + " "
//                        + drinkItemsSpinner.getSelectedItem().toString()) + "with " +
//                        milkAmountSpinner.getSelectedItem().toString() + " milks & "
//                        + sugarAmountSpinner.getSelectedItem().toString() + " sugars"
//                        , Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        addToOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, ("Your Order. A " + drinkSizeSpinner.getSelectedItem().toString() + " "
                        + drinkItemsSpinner.getSelectedItem().toString()) + " with  " +
                 milkAmountSpinner.getSelectedItem().toString() + " extra milk & "
                        + sugarAmountSpinner.getSelectedItem().toString() + " sugars "
                     , Snackbar.LENGTH_LONG)
                       .setAction("Action", null).show();
            }
        });
        return orderPageView;
    }


    AdapterView.OnItemSelectedListener drinkSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            switch (position) {
                case 0:
                    Toast.makeText(getActivity().getApplicationContext(), "Americano", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    Toast.makeText(getActivity().getApplicationContext(), "Cappucino", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    Toast.makeText(getActivity().getApplicationContext(), "Espresso", Toast.LENGTH_SHORT).show();
                    break;
                case 3:
                    Toast.makeText(getActivity().getApplicationContext(), "Latte", Toast.LENGTH_SHORT).show();
                    break;
                case 4:
                    Toast.makeText(getActivity().getApplicationContext(), "Macchiato", Toast.LENGTH_SHORT).show();
                    break;
                case 5:
                    Toast.makeText(getActivity().getApplicationContext(), "Mocha", Toast.LENGTH_SHORT).show();
                    break;
                case 6:
                    Toast.makeText(getActivity().getApplicationContext(), "Tea", Toast.LENGTH_SHORT).show();
                    break;
                case 7:
                    Toast.makeText(getActivity().getApplicationContext(), "Tea", Toast.LENGTH_SHORT);

                    break;
                default:
                    Toast.makeText(getActivity().getApplicationContext(), "Nothing Selected", Toast.LENGTH_SHORT).show();

                    break;
            }


        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            Toast.makeText(getActivity().getApplicationContext(), "Nothing Selected", Toast.LENGTH_SHORT).show();

        }
    };

    AdapterView.OnItemSelectedListener sizeSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            switch (position) {

                    case 0:
                        Toast.makeText(getActivity().getApplicationContext(), "Small", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Toast.makeText(getActivity().getApplicationContext(), "Medium", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(getActivity().getApplicationContext(), "Large", Toast.LENGTH_SHORT).show();
                        break;
            }


        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            Toast.makeText(getActivity().getApplicationContext(), "Nothing Selected", Toast.LENGTH_SHORT).show();

        }
    };



    AdapterView.OnItemSelectedListener milkSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            switch (position) {

                 case 0:
                    Toast.makeText(getActivity().getApplicationContext(), "No Milk", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    Toast.makeText(getActivity().getApplicationContext(), "One Milk", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    Toast.makeText(getActivity().getApplicationContext(), "Two Milks", Toast.LENGTH_SHORT).show();
                    break;
                case 3:
                    Toast.makeText(getActivity().getApplicationContext(), "Three Milks", Toast.LENGTH_SHORT).show();
                    break;
                case 4:
                    Toast.makeText(getActivity().getApplicationContext(), "Four Milks", Toast.LENGTH_SHORT).show();
                    break;
                case 5:
                    Toast.makeText(getActivity().getApplicationContext(), "Five Milks", Toast.LENGTH_SHORT).show();
                    break;
            }


        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            Toast.makeText(getActivity().getApplicationContext(), "Nothing Selected", Toast.LENGTH_SHORT).show();

        }
    };


    AdapterView.OnItemSelectedListener sugarSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            switch (position) {

                case 0:
                    Toast.makeText(getActivity().getApplicationContext(), "No Sugar", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    Toast.makeText(getActivity().getApplicationContext(), "One Sugar", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    Toast.makeText(getActivity().getApplicationContext(), "Two Sugars", Toast.LENGTH_SHORT).show();
                    break;
                case 3:
                    Toast.makeText(getActivity().getApplicationContext(), "Three Sugars", Toast.LENGTH_SHORT).show();
                    break;
                case 4:
                    Toast.makeText(getActivity().getApplicationContext(), "Four Sugars", Toast.LENGTH_SHORT).show();
                    break;
                case 5:
                    Toast.makeText(getActivity().getApplicationContext(), "Five Sugars", Toast.LENGTH_SHORT).show();
                    break;
            }


        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            Toast.makeText(getActivity().getApplicationContext(), "Nothing Selected", Toast.LENGTH_SHORT).show();

        }
    };




}









