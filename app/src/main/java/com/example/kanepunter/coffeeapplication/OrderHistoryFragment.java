package com.example.kanepunter.coffeeapplication;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.HttpAuthHandler;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHistoryFragment extends Fragment {


    private  String TAG = OrderHistoryFragment.class.getSimpleName();
    private View orderHistoryView;

    private ProgressDialog progressDialog;
    private ListView listView;

    private static String jsonURL = "https://api.myjson.com/bins/176nlx";

    ArrayList<HashMap<String, String>> orderList;

    public OrderHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View oHV = inflater.inflate(R.layout.fragment_order_history,container,false);
        listView = (ListView) oHV.findViewById(R.id.order_history_lv);
        orderList = new ArrayList<>();
        return oHV;
    }

    private class getOrderHistory extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();


            progressDialog = new ProgressDialog(orderHistoryView.getContext());
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpHandler handler = new HttpHandler();
            String jsonString = handler.makeServiceCall(jsonURL);
            Log.e(TAG,"Response from URL: " + jsonString);
            if (jsonString != null) {
                try {
                    JSONObject ordersObj = new JSONObject(jsonString);
                    JSONArray orders = ordersObj.getJSONArray("order");

                    for(int i = 0; i < orders.length(); i++) {
                        JSONObject o = orders.getJSONObject(i);

                        String orderID = o.getString("id");
                        String drinkName = o.getString("drink");
                        String drinkSize = o.getString("drinkSize");
                        String milks = o.getString("milk");
                        String sugars = o.getString("sugars");

                        HashMap<String,String> order = new HashMap<>();

                        order.put("id",orderID);
                        order.put("drink", drinkName);
                        order.put("drinkSize", drinkSize);
                        order.put("milk", milks);
                        order.put("sugars", sugars);

                        orderList.add(order);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "JSON Parsing Error" + e.getMessage());
                }
            }
            return null;
    }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing())
                progressDialog.dismiss();

            ListAdapter listAdapter = new SimpleAdapter(
                    orderHistoryView.getContext(),orderList,R.layout.list_item,
                    new String[]{"id","drink", "drinkSize"},
                    new int[]{ R.id.order_id_list, R.id.drink_ordered_list, R.id.size_ordered_list});

            listView.setAdapter(listAdapter);
        }
    }
}
